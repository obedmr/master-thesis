References
==========

Books
-----
- [Thesis Projects](https://drive.google.com/file/d/0B5ub6o7ifOdDczE4Z1lfRHVGMTA/view?usp=sharing)
- [Virtual Reality for Physical and Motor Rehabilitation](https://play.google.com/store/books/details?id=r5EpBAAAQBAJ&source=productsearch&utm_source=HA_Desktop_US&utm_medium=SEM&utm_campaign=PLA&pcampaignid=MKTAD0930BO1&gclid=COmS0-jJjc8CFcffDQodyBkBcA&gclsrc=ds) 
- [Virtual Reality Enhanced Robotic Systems for Disability Rehabilitation](http://techbus.safaribooksonline.com/book/biotechnology/9781466697409)


Articles
--------

### Virtual Reality and Image processing

- **2003** **DONE** - [Recent developments in human motion analysis](./articles.md)
- **2004** **DONE** - [Virtual reality and physical rehabilitation: a new toy or a new research and rehabilitation tool?](./articles.md)
- **2004** **DONE** - [Motor rehabilitation using virtual reality](articles.md#motor-rehabilitation-using-virtual-reality)
- **2006** - [A-survey-of-advances-in-vision-based-human-motion-capture-and-analysis]()
- **2007** - [Design of a marker based human motion tracking system]()
- **2008** - [Human motion tracking for rehabilitation—A survey]()
- **2012** - [A Feasibility Study of an Upper Limb Rehabilitation System Using  Kinect and Computer Games]()
- **2012** - [Biomechanical Validation of Upper-Body and Lower-Body Joint Movements of Kinect Motion Capture Data for Rehabilitation Treatments]()
- **2012** - [Haptic Augmented Reality to Monitor Human Arm's Stiffness in Rehabilitation]()
- **2013** - [Computer Aided Rehabilitation  for Patients with Rheumatoid Arthritis]()
- **2013** - [Non inmmersive Virtual Reality for fine motor rehabilitation of functional activities in individuals with chronic stroke (a review)]()
- **2014** - [A Kinect-based system for cognitive rehabilitation  exercises monitoring]()

### Physical Therapy
- **2008** - **DONE** - [What Do Motor “Recovery” and “Compensation” Mean in Patients Following Stroke?]()
- **2014** - [Clinical Experience Using a 5-Week Treadmill Training Program With Virtual Reality to Enhance Gait in an Ambulatory Physical Therapy Service]()
- **2014** - [Patients' Use of a Home-Based Virtual Reality System to Provide Rehabilitation of the Upper Limb Following Stroke]()
- **2015** - [Considerations in the Efficacy and Effectiveness of Virtual Reality Interventions for Stroke Rehabilitation: Moving the Field Forward]()
- **2015** - [Does a Virtual Reality based dance training paradigm increase balance control in chronic stroke survivors (a preliminary study)]()
- **2015** - **DONE** - [Trends in Physical Medicine and Rehabilitation Publications Over the Past 16 Years](./articles.md)
- **2015** - [Emergence of Virtual Reality as a Tool for Upper Limb Rehabilitation: Incorporation of Motor Control and Motor Learning Principles]()
- **2016** - [A Further Step to Develop Patient-Friendly Implementation Strategies for Virtual Reality–Based Rehabilitation in Patients With Acute Stroke]()
- **2016** - [Effect of Virtual Reality Training on Balance and Gait Ability in Patients With Stroke: Systematic Review and Meta-Analysis]()
- **2016** - **DONE** - [Virtual Reality for Stroke Rehabilitation]()

Websites
--------
- [How Virtual Reality Will Change Physical Therapy Forever](http://www.inc.com/aj-agrawal/how-virtual-reality-will-change-physical-therapy-forever.html)
- [Virtual Reality in Stroke Rehabilitation](https://nydnrehab.com/treatment-methods/neurorehab/virtual-reality-in-stroke-rehabilitation/)