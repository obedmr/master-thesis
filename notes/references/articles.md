Articles reviews
================

## 2003 - Recent developments in human motion analysis

- This papers talks about the use of visual analysis and the range of topics in computer vision. Basically, it will talk about the main analysis steps: human motion analysis system (also known as detection), tracking and activity understanding.

- Below some applications that have been commonly focused:
  - Visual survillance: Common survillance solutions used to work in a "after the fact" way, it means, there were video-taped recordings of the scenes and when a problem occurs, security team goes to the video archive and manually looks for the issue. There's a strong need of real-time analysis. Detect isssues in real-time with the help of computer vision.
  - Advanced user interfaces: More information can be obtained from the environment, human gestures and movements. It increases the User experience when you consider all these aspects.
  - Motion-based diagnosis and identification: Tracking human movements for analysis and training. (Many applications on sports, medical and choreography)
 
- Computer graphics results as a good ally on all this, it provides visual representation of the information that is being obtained from computer vision means.

- Previous surveys showed that computer vision focus is going on 3 main directions:
  - Motion analysis involving human body parts
  - Tracking moving human fron single view or multiple camera perspectives
  - Recognize human activities from image sequences

- A general framework for human motion analysis
```
         +--------------------------------------+
  Motion analysis
         |                              Human Detection
         |                              (Low level vision)
         |                                      
  Object Classification
         |                                        
         +--------------------------------------+
         |                                      
  Human Tracking                        Human tracking
         |                              (Intermediate-level vision)      
         |                                      
         +--------------------------------------+
  Action Recognition
         |                                      
         |                              Behavior Understanding
         |                              (High-level vision)
  Semantic Description
         +--------------------------------------+
```

- Detection phase: Also known as the segmentation of featured regions.
  - Motion segmentation - this is an intensive-work test, specially on video sequences like (vehicles traffic or walking people). It also represents a challenge when there are changes in environment like weather or lighting. Below some methods for motion detection.
    - Background substraction: it's one of the classics, It's a differentiation from a reference image and the current one. It's really sensitive to minor changes, that's why it requires adaptive models to support weather and lighting changes.
    - Statistical methods: To extract changes in the background it make uses of means and other statical method to get differiatiors. It as a good robustness to noise, shadows, change of lighting, etc.
    - Temporal differencing: It presents a more adaptive and dynamic method but sometimes it's poor on image features extracting.
    - Optical flow: Used to describe coherent motion of points or features between image frames.
  - Object classification - Once you have detected the objects, it's required to classify those objects as humans, cars, etc
    - Shape-based classification: Depending of the form, we can classify as a box, a vehicle, a human or any other object. Here's where neural networks can be useful for classification and patterns findinf.
    - Motion-based classification: Objects can also classified based on the way they move, in a serie of frames it's possible to identivy movements patterns and decide which object is there.
- Tracking phase: It helps to pose estimation and action recognition. Tracking over time will involve a matching technique between different frames of the video by the use of features like points, lines or blobs. When talking about tracking, there are different approach depending of the objects we want to track, for example: human body parts, the complete body, groups of objects, moving or stationary camera, etc
  - Model-based tracking
    - Stick figure
    - 2-D contour
    - Volumetric models
  - Region-based tracking
  - Active-contour-based tracking
  - Feature-based tracking
- Behavior understanding phase: After detecting and tracking objects, it's the most challenging part. Understanding the meaning of action. On humans motion analysis, it will involve a serie of mechanisms for it, and this a long-term research field. Many patterns recognition technoques can be applied here.
  - Techniques
    - Dynamic time warping - Commonly used on speech recognition, template-based dynamic programming matching technique
    - Hidden Markov models: time-varying data with spatio-temporal variability
    - Neural networks
  - Action recognition
    - Template matching
    - State-space approaches
  - Semantic description
- Discussion and found issues
  - Segmentation: there's a need of more accurate segmentation. Dynamic environment represent many factors that may alter the proper segmentation
  - Occlusion handling
  - 3-D modeling and tracking
  - Use of multiple cameras for better environment understanding, but it increases complexity
  - Action understanding
   - Performance evaluation
  

## 2004 - Virtual reality and physical rehabilitation: a new toy or a new research and rehabilitation tool?

- Virtual Reality is becoming in a popular application for **rehabilitation** and **motor control** research.
- On early 1990's, physical rehabilitation with Virtual Reality research started
 
- Examples of virtual reality applications:
  - Virtual 3D environment where the user feels he/she is inside the scene.
  - 4 forms of virtal environments
    - Head Mounted Display
    - Augmented
    - Fish Tank
    - Projection based

- Why use a virtual world for rehabilitation?
  - VR offers the opportunity to bring complexity of the real world to a controlled environment that can be a laboratory.
  - Strengths of VR
    - Ecological validity
    - Stimulus and response modifications
    - Safe testing and training environment
    - Graduated exposure to stimuli
    - Ability to distract oe augment the perform's attention
    - Motivation
    - Assisting therapeutic intervention

## 2004 - Motor rehabilitation using virtual reality

- VR provides a medium for enhacing the rehabilation experience. Alongside other technological components they achieve the assistance and documentation of the progress in therapeutic intervention and benefits both, therapists and patients.
- User experience improves and allows the users to feel inside the environment. It provides the capability of interaction on real-time through multiple visual/no-visual interfaces.

- One of major goals of rehabilitation is to make quantitative and qualitative improvement in order to make users more independent and healty each day.
- There are 3 main determinants of motor recovery:
  1. Early intervention
  2. Task-oriented training
  3. Repetition intensity

- It's important to have control in the performed tasks, the practice of motor tasks should improve the user's capabilities, but it's important to have the right data and user the proper sensory inputs in order to do a good manipulation of performance. Posture control is something that is important in order to avoid serious injuries.
 
- VR provides an ecological alternative for therapeutic treatments. A good advantage of this is that it can enhance control over stimulus delivery and progress measurements.
 
- VR can help from diferent angles like multidimensional environments, multisensory, Computer-generated environments and real-time tracking. It adds values to standarization of treatments and training protocols.
 
- Interaction is key to make the user feel part of the environment. There are several visual interfaces to prove a better immersion. Haptic interface devices provide a sense of touch. Haptic information can be generated from devices like: gloves, pens, joysticks and exoskeletons. They provide the feeling of a variaty of textures as well changes on them.
 
- Sense of presence in, this is something that VR can achieve, it's the feeling of being in an environment even if not physically present but feels part of it.
 
- A big difference between the use of VE and Real environments. When using VR, it's required a good intelligence in order to identify when the user is performing better and at the same time make adjustments on treatments with more dificult tasks that force improvements in rehabilitation.
 
- Transfer of training is key in order to achieve the rehabilitation goal. It requires good recording and intelligence to find patterns and progress on process, and in some cases, make adjustments to the tasks in order to avoid failure, it's important to do a good analysis between real-world and virtual environments and make sure good practices are learned from both sides.
 
- VR applications for balance and posture control are presented as a success story. It implements realistic UI which makes participants more confortable and they demonstrated that they enjoyed the practice. It's important to consider the fact that VR needs to be as much friendly as possible.
 
- Locomotion applications are presented, most of the applications displayed virtual environments that helped the participants to anticipate steps, avoid road blocks and improve confidence on walking.
 
- Upper and Lower extremity function applications with virtual environments that make participants confident about doing tasks in virtual environments. One of the examples is similar to this thesis proposal, there's a display in front and participants needs to follow the virtual trainer that is in front. The missing part could be the good validation and recording of progress and performance improvements.

- In Exercise and pain tolerance, VR is used as a positive distractive technique. It reduced anxiety and reductions on self-report of pain.
 
- Assesments as a requirement for improving the rehabilitation process, there's a strong need of developing more technology on this.
 
- Regarding the access to rehabilitation, many patients don't finish the treatments or don't take advantage of it. With VR, it offers a more attractive way of treatment and makes the participants happier when they see they're having fun and improving. It's a win-win.

### 2014 - Patients' Use of Home-based Virtual Reality System to Provide Rehabilitation of the Upper Limb Following Stroke


### 2015 - Trends in Physical Medicine and Rehabilitation Publications Over the Past 16 Years

- In last 16 years, number of publications about physical medicine and rehabilitation has increased more than pediatric publications.
- The World Health Organization estimates that globally 1 billion people or 15% of the world's population expecience disabilities, of whom 110 to 190 million people (2% of the world's population) experience severe or extreme diffuculties in functioning.

### 2016 - Virtual Reality for Stroke Rehabilitation
- This is about finding and application of Cochrane reviews and other evidence pertitent to the practice of physical therapy.
- Stroke is caused by a disruption of the blood supply to the brain because an artery to the brain is either blocked or bursts, causing damage to the brain tissue.
- It causes several issues on mobility such as weakness and loss of coordination.
- Many treatmentment options are available, repetitive task specific training is the common one, but it results challending due the fact of a high suppervised and well tracked recovery process in order to have good results on therapy treatment.
- VR introduces a high dose of repetitive task specific training with his capability of reproduce or represent real-life scenarios in a virtual way.
- VR also provides concurrent feeback which helps to improve the way exercises are instructed.
- This paper introduces a full detailed case of study where an experiment with VR as rehabilitation solution were used by stroke patients and how it provided good results to the treatment.
- It shows the use of video games for the treatments
- VR was used alongside the usual treatment tasks.
- It demonstrated a high affection to the VR systems that were used.