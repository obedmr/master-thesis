Thesis Report Document
======================
This is a Latex-written thesis document. Document structure is described below.

Requirements
------------
- Docker


How to compile it
-----------------
```bash
  cd src/
  make
```

Result document will be located at:
```
  src/aux/thesis.pdf
```

Automated publishment can be found at:
http://share.obedmr.com/thesis/

Report Structure
----------------

- Title page
- Abstract
- Acknowledgements
- Table of contents
- List of figures and tables

**Part 1: Introduction**
- Chapter 1: Introduction
- Chapter 2: Background
- Chapter 3: Problem description and problem statement

**Part 2: Main body - Describes the thesis work**
- Chapter 4: Theory (advanced preliminaries and/or theorical part of solution)
- Chapter 5: Description of approach and method(s) to solve the problem
- Chapter 6: Result analysis

**Part 3: Wrap-up**
- Chapter 7: Related work
- Chapter 8: Conclusions

- References (or Bibliography)
- Appendices (optional)
