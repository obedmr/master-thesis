##################################################
#
# 3D Therapeutic templates matching
#
# Exercise: ex2
#
##################################################
#
# Exercise 2D Paths Match
#
# Instructor Points : 74894
#
# Matched Points    : 55852
# Missing Points    : 19042
# Extra Points      : 71757
# Success Rate      : 74.5747%
#
##################################################
#
# Depth Data Match with (-20,20) depth threshold
#
# Instructor Points : 74887
#
# Matched depth points: 45656
# Go back points      : 12377
# Go forward points   : 16861
# Success Rate        : 60.9665%
#
##################################################
