##################################################
#
# 3D Therapeutic templates matching
#
# Exercise: ex4
#
##################################################
#
# Exercise 2D Paths Match
#
# Instructor Points : 54879
#
# Matched Points    : 32817
# Missing Points    : 22062
# Extra Points      : 55582
# Success Rate      : 59.7988%
#
##################################################
#
# Depth Data Match with (-20,20) depth threshold
#
# Instructor Points : 54843
#
# Matched depth points: 32210
# Go back points      : 13179
# Go forward points   : 9483
# Success Rate        : 58.7313%
#
##################################################
