# Master in Computer Science Thesis

- Author: Obed Nehemias Munoz Reynoso
- Thesis Advisor: Dr. Miguel Bernal Marin 

This repository holds my master degree thesis work. Documentation, Thesis report and source code will be added here.

## Project structure

There are 3 main sections, each section has its own directory and more details inside.

- notes - Articles and book reviews
- report - Latex-generated thesis report source cod3e
- src - project source code (algorithms and project's development tools)

## Important links

- [Thesis Project page](https://gitlab.com/obedmr/master-thesis)
- [Issues/Tasks Tracking](https://gitlab.com/obedmr/master-thesis/issues)
- [Progress tracking board](https://gitlab.com/obedmr/master-thesis/board)
- [Thesis document release page](http://share.obedmr.com/thesis/)